package com.youssefelsa3ed.thetask.models.GeneralResponse;

public class Error {

    private String msg;
    private Throwable throwable;

    public Error(String msg, Throwable throwable) {
        this.msg = msg;
        this.throwable = throwable;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Throwable getThrowable() {
        return throwable;
    }

    public void setThrowable(Throwable throwable) {
        this.throwable = throwable;
    }
}
