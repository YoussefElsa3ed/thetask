package com.youssefelsa3ed.thetask.models.payRollResponse;

import com.google.gson.annotations.SerializedName;

public class EmployeeData {

	@SerializedName("CONTRACTSTDATE")
	private String cONTRACTSTDATE;

	@SerializedName("SAL_VALUE_NET")
	private double sALVALUENET;

	@SerializedName("SEC_NAME_AR")
	private String sECNAMEAR;

	@SerializedName("CUSTOM_ID")
	private double cUSTOMID;

	@SerializedName("SAL_VALUE_D")
	private double sALVALUED;

	@SerializedName("FRACTIONNAME_AR")
	private String fRACTIONNAMEAR;

	@SerializedName("EMP_PIC")
	private String eMPPIC;

	@SerializedName("SEC_NAME_EN")
	private String sECNAMEEN;

	@SerializedName("JOBCODE")
	private double jOBCODE;

	@SerializedName("SAL_VALUE_A")
	private double sALVALUEA;

	@SerializedName("COMP_DESC_D_EN")
	private String cOMPDESCDEN;

	@SerializedName("SAL_COMP_CODE_D")
	private double sALCOMPCODED;

	@SerializedName("CURRSYMBOL_EN")
	private String cURRSYMBOLEN;

	@SerializedName("COMP_DESC_D_AR")
	private String cOMPDESCDAR;

	@SerializedName("MAR_STATUS_AR")
	private String mARSTATUSAR;

	@SerializedName("SAL_COMP_CODE_A")
	private double sALCOMPCODEA;

	@SerializedName("CURRSYMBOL_AR")
	private String cURRSYMBOLAR;

	@SerializedName("STATUS_EN")
	private String sTATUSEN;

	@SerializedName("ATM_ACCOUNT")
	private String aTMACCOUNT;

	@SerializedName("FRACTIONNAME_EN")
	private String fRACTIONNAMEEN;

	@SerializedName("STATUSNAME_AR")
	private String sTATUSNAMEAR;

	@SerializedName("MAR_STATUS_EN")
	private String mARSTATUSEN;

	@SerializedName("STATUS_AR")
	private String sTATUSAR;

	@SerializedName("EMP_GENDUR")
	private String eMPGENDUR;

	@SerializedName("EMP_DATA_AR")
	private String eMPDATAAR;

	@SerializedName("COMP_DESC_A_EN")
	private String cOMPDESCAEN;

	@SerializedName("STATUSNAME_EN")
	private String sTATUSNAMEEN;

	@SerializedName("EMP_ID")
	private int eMPID;

	@SerializedName("JOBNAME_EN")
	private String jOBNAMEEN;

	@SerializedName("EMP_DATA_EN")
	private String eMPDATAEN;

	@SerializedName("COMP_DESC_A_AR")
	private String cOMPDESCAAR;

	@SerializedName("JOBNAME_AR")
	private String jOBNAMEAR;

	public void setCONTRACTSTDATE(String cONTRACTSTDATE){
		this.cONTRACTSTDATE = cONTRACTSTDATE;
	}

	public String getCONTRACTSTDATE(){
		return cONTRACTSTDATE;
	}

	public void setSALVALUENET(double sALVALUENET){
		this.sALVALUENET = sALVALUENET;
	}

	public double getSALVALUENET(){
		return sALVALUENET;
	}

	public void setSECNAMEAR(String sECNAMEAR){
		this.sECNAMEAR = sECNAMEAR;
	}

	public String getSECNAMEAR(){
		return sECNAMEAR;
	}

	public void setCUSTOMID(double cUSTOMID){
		this.cUSTOMID = cUSTOMID;
	}

	public double getCUSTOMID(){
		return cUSTOMID;
	}

	public void setSALVALUED(double sALVALUED){
		this.sALVALUED = sALVALUED;
	}

	public double getSALVALUED(){
		return sALVALUED;
	}

	public void setFRACTIONNAMEAR(String fRACTIONNAMEAR){
		this.fRACTIONNAMEAR = fRACTIONNAMEAR;
	}

	public String getFRACTIONNAMEAR(){
		return fRACTIONNAMEAR;
	}

	public void setEMPPIC(String eMPPIC){
		this.eMPPIC = eMPPIC;
	}

	public String getEMPPIC(){
		return eMPPIC;
	}

	public void setSECNAMEEN(String sECNAMEEN){
		this.sECNAMEEN = sECNAMEEN;
	}

	public String getSECNAMEEN(){
		return sECNAMEEN;
	}

	public void setJOBCODE(double jOBCODE){
		this.jOBCODE = jOBCODE;
	}

	public double getJOBCODE(){
		return jOBCODE;
	}

	public void setSALVALUEA(double sALVALUEA){
		this.sALVALUEA = sALVALUEA;
	}

	public double getSALVALUEA(){
		return sALVALUEA;
	}

	public void setCOMPDESCDEN(String cOMPDESCDEN){
		this.cOMPDESCDEN = cOMPDESCDEN;
	}

	public String getCOMPDESCDEN(){
		return cOMPDESCDEN;
	}

	public void setSALCOMPCODED(double sALCOMPCODED){
		this.sALCOMPCODED = sALCOMPCODED;
	}

	public double getSALCOMPCODED(){
		return sALCOMPCODED;
	}

	public void setCURRSYMBOLEN(String cURRSYMBOLEN){
		this.cURRSYMBOLEN = cURRSYMBOLEN;
	}

	public String getCURRSYMBOLEN(){
		return cURRSYMBOLEN;
	}

	public void setCOMPDESCDAR(String cOMPDESCDAR){
		this.cOMPDESCDAR = cOMPDESCDAR;
	}

	public String getCOMPDESCDAR(){
		return cOMPDESCDAR;
	}

	public void setMARSTATUSAR(String mARSTATUSAR){
		this.mARSTATUSAR = mARSTATUSAR;
	}

	public String getMARSTATUSAR(){
		return mARSTATUSAR;
	}

	public void setSALCOMPCODEA(double sALCOMPCODEA){
		this.sALCOMPCODEA = sALCOMPCODEA;
	}

	public double getSALCOMPCODEA(){
		return sALCOMPCODEA;
	}

	public void setCURRSYMBOLAR(String cURRSYMBOLAR){
		this.cURRSYMBOLAR = cURRSYMBOLAR;
	}

	public String getCURRSYMBOLAR(){
		return cURRSYMBOLAR;
	}

	public void setSTATUSEN(String sTATUSEN){
		this.sTATUSEN = sTATUSEN;
	}

	public String getSTATUSEN(){
		return sTATUSEN;
	}

	public void setATMACCOUNT(String aTMACCOUNT){
		this.aTMACCOUNT = aTMACCOUNT;
	}

	public String getATMACCOUNT(){
		return aTMACCOUNT;
	}

	public void setFRACTIONNAMEEN(String fRACTIONNAMEEN){
		this.fRACTIONNAMEEN = fRACTIONNAMEEN;
	}

	public String getFRACTIONNAMEEN(){
		return fRACTIONNAMEEN;
	}

	public void setSTATUSNAMEAR(String sTATUSNAMEAR){
		this.sTATUSNAMEAR = sTATUSNAMEAR;
	}

	public String getSTATUSNAMEAR(){
		return sTATUSNAMEAR;
	}

	public void setMARSTATUSEN(String mARSTATUSEN){
		this.mARSTATUSEN = mARSTATUSEN;
	}

	public String getMARSTATUSEN(){
		return mARSTATUSEN;
	}

	public void setSTATUSAR(String sTATUSAR){
		this.sTATUSAR = sTATUSAR;
	}

	public String getSTATUSAR(){
		return sTATUSAR;
	}

	public void setEMPGENDUR(String eMPGENDUR){
		this.eMPGENDUR = eMPGENDUR;
	}

	public String getEMPGENDUR(){
		return eMPGENDUR;
	}

	public void setEMPDATAAR(String eMPDATAAR){
		this.eMPDATAAR = eMPDATAAR;
	}

	public String getEMPDATAAR(){
		return eMPDATAAR;
	}

	public void setCOMPDESCAEN(String cOMPDESCAEN){
		this.cOMPDESCAEN = cOMPDESCAEN;
	}

	public String getCOMPDESCAEN(){
		return cOMPDESCAEN;
	}

	public void setSTATUSNAMEEN(String sTATUSNAMEEN){
		this.sTATUSNAMEEN = sTATUSNAMEEN;
	}

	public String getSTATUSNAMEEN(){
		return sTATUSNAMEEN;
	}

	public void setEMPID(int eMPID){
		this.eMPID = eMPID;
	}

	public int getEMPID(){
		return eMPID;
	}

	public void setJOBNAMEEN(String jOBNAMEEN){
		this.jOBNAMEEN = jOBNAMEEN;
	}

	public String getJOBNAMEEN(){
		return jOBNAMEEN;
	}

	public void setEMPDATAEN(String eMPDATAEN){
		this.eMPDATAEN = eMPDATAEN;
	}

	public String getEMPDATAEN(){
		return eMPDATAEN;
	}

	public void setCOMPDESCAAR(String cOMPDESCAAR){
		this.cOMPDESCAAR = cOMPDESCAAR;
	}

	public String getCOMPDESCAAR(){
		return cOMPDESCAAR;
	}

	public void setJOBNAMEAR(String jOBNAMEAR){
		this.jOBNAMEAR = jOBNAMEAR;
	}

	public String getJOBNAMEAR(){
		return jOBNAMEAR;
	}
}