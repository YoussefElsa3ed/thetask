package com.youssefelsa3ed.thetask.models.GeneralResponse;

import androidx.lifecycle.MutableLiveData;

public class GeneralResponseLiveData<T> extends MutableLiveData<GeneralResponse<T>> {

    public void postLoading() {
        postValue(new GeneralResponse<T>().loading());
    }

    public void postError(Error error) {
        postValue(new GeneralResponse<T>().error(error));
    }

    public void postSuccess(T data) {
        postValue(new GeneralResponse<T>().success(data));
    }


}
