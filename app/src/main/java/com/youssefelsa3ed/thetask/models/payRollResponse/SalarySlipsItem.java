package com.youssefelsa3ed.thetask.models.payRollResponse;

import androidx.recyclerview.widget.DiffUtil;

import com.google.gson.annotations.SerializedName;

public class SalarySlipsItem {

	@SerializedName("EMP_ID")
	private int eMPID;

	@SerializedName("SAL_COMP_CODE")
	private int sALCOMPCODE;

	@SerializedName("COMP_DESC_EN")
	private String cOMPDESCEN;

	@SerializedName("SAL_VALUE")
	private double sALVALUE;

	@SerializedName("COMP_DESC_AR")
	private String cOMPDESCAR;

	@SerializedName("SAL_COMP_TYPE")
	private int sALCOMPTYPE;

	private boolean isAllowance;

	public void setEMPID(int eMPID){
		this.eMPID = eMPID;
	}

	public int getEMPID(){
		return eMPID;
	}

	public void setSALCOMPCODE(int sALCOMPCODE){
		this.sALCOMPCODE = sALCOMPCODE;
	}

	public int getSALCOMPCODE(){
		return sALCOMPCODE;
	}

	public void setCOMPDESCEN(String cOMPDESCEN){
		this.cOMPDESCEN = cOMPDESCEN;
	}

	public String getCOMPDESCEN(){
		return cOMPDESCEN;
	}

	public void setSALVALUE(double sALVALUE){
		this.sALVALUE = sALVALUE;
	}

	public double getSALVALUE(){
		return sALVALUE;
	}

	public void setCOMPDESCAR(String cOMPDESCAR){
		this.cOMPDESCAR = cOMPDESCAR;
	}

	public String getCOMPDESCAR(){
		return cOMPDESCAR;
	}

	public void setSALCOMPTYPE(int sALCOMPTYPE){
		this.sALCOMPTYPE = sALCOMPTYPE;
	}

	public int getSALCOMPTYPE(){
		return sALCOMPTYPE;
	}

	public boolean isAllowance() {
		return isAllowance;
	}

	public void setAllowance(boolean allowance) {
		isAllowance = allowance;
	}

	public static final DiffUtil.ItemCallback<SalarySlipsItem> DIFF_CALLBACK = new DiffUtil.ItemCallback<SalarySlipsItem>() {
		@Override
		public boolean areItemsTheSame(SalarySlipsItem oldItem, SalarySlipsItem newItem) {
			return oldItem.getSALCOMPCODE() == newItem.getSALCOMPCODE();
		}

		@Override
		public boolean areContentsTheSame(SalarySlipsItem oldItem, SalarySlipsItem newItem) {
			return oldItem.getCOMPDESCAR().equals(newItem.getCOMPDESCAR());
		}
	};

}