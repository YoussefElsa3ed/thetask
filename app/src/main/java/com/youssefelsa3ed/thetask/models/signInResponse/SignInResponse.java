package com.youssefelsa3ed.thetask.models.signInResponse;

import com.google.gson.annotations.SerializedName;

public class SignInResponse{

	@SerializedName("EnglishMessage")
	private String englishMessage;

	@SerializedName("ArabicMessage")
	private String arabicMessage;

	@SerializedName("UserRole")
	private Object userRole;

	@SerializedName("Activation")
	private boolean activation;

	@SerializedName("Token")
	private String token;

	@SerializedName("AccountRole")
	private Object accountRole;

	@SerializedName("user")
	private Object user;

	@SerializedName("Code")
	private int code;

	@SerializedName("Success")
	private boolean success;

	public void setEnglishMessage(String englishMessage){
		this.englishMessage = englishMessage;
	}

	public String getEnglishMessage(){
		return englishMessage;
	}

	public void setArabicMessage(String arabicMessage){
		this.arabicMessage = arabicMessage;
	}

	public String getArabicMessage(){
		return arabicMessage;
	}

	public void setUserRole(Object userRole){
		this.userRole = userRole;
	}

	public Object getUserRole(){
		return userRole;
	}

	public void setActivation(boolean activation){
		this.activation = activation;
	}

	public boolean isActivation(){
		return activation;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}

	public void setAccountRole(Object accountRole){
		this.accountRole = accountRole;
	}

	public Object getAccountRole(){
		return accountRole;
	}

	public void setUser(Object user){
		this.user = user;
	}

	public Object getUser(){
		return user;
	}

	public void setCode(int code){
		this.code = code;
	}

	public int getCode(){
		return code;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}
}