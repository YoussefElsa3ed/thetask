package com.youssefelsa3ed.thetask.models.payRollResponse;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Payroll{

	@SerializedName("Employee")
	private List<EmployeeData> employee;

	@SerializedName("Allowences")
	private List<SalarySlipsItem> allowances;

	@SerializedName("Deduction")
	private List<SalarySlipsItem> deduction;

	@SerializedName("Date")
	private String date;

	public void setEmployee(List<EmployeeData> employee){
		this.employee = employee;
	}

	public List<EmployeeData> getEmployee(){
		return employee;
	}

	public void setAllowances(List<SalarySlipsItem> allowances){
		this.allowances = allowances;
	}

	public List<SalarySlipsItem> getAllowances(){
		return allowances;
	}

	public void setDeduction(List<SalarySlipsItem> deduction){
		this.deduction = deduction;
	}

	public List<SalarySlipsItem> getDeduction(){
		return deduction;
	}

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public double getTotalAllowances(){
		double sum = 0;
		for (SalarySlipsItem item : allowances) {
			sum += item.getSALVALUE();
		}
		return sum;
	}

	public double getTotalDeductions(){
		double sum = 0;
		for (SalarySlipsItem item : deduction) {
			sum -= item.getSALVALUE();
		}
		return sum;
	}
}