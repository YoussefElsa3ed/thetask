package com.youssefelsa3ed.thetask.models.payRollResponse;

import com.google.gson.annotations.SerializedName;

public class PayRollResponse{

	@SerializedName("Payroll")
	private Payroll payroll;

	@SerializedName("EnglishMessage")
	private String englishMessage;

	@SerializedName("ArabicMessage")
	private String arabicMessage;

	@SerializedName("Activation")
	private boolean activation;

	@SerializedName("Success")
	private boolean success;

	public void setPayroll(Payroll payroll){
		this.payroll = payroll;
	}

	public Payroll getPayroll(){
		return payroll;
	}

	public void setEnglishMessage(String englishMessage){
		this.englishMessage = englishMessage;
	}

	public String getEnglishMessage(){
		return englishMessage;
	}

	public void setArabicMessage(String arabicMessage){
		this.arabicMessage = arabicMessage;
	}

	public String getArabicMessage(){
		return arabicMessage;
	}

	public void setActivation(boolean activation){
		this.activation = activation;
	}

	public boolean isActivation(){
		return activation;
	}

	public void setSuccess(boolean success){
		this.success = success;
	}

	public boolean isSuccess(){
		return success;
	}
}