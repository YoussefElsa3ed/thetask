package com.youssefelsa3ed.thetask.utills;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.youssefelsa3ed.thetask.models.signInResponse.SignInResponse;


public class SharedPrefManager {
    private final String SHARED_PREF_NAME = "the_task";
    private final String LOGIN_STATUS = "login_status";
    private final String USER_DATA = "user_data";
    private final String TOKEN = "token";

    private Context mContext;

    public SharedPrefManager(Context mContext) {
        this.mContext = mContext;
    }

    public Boolean getLoginStatus() {
        final SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        return sharedPreferences.getBoolean(LOGIN_STATUS, false);
    }

    private void setLoginStatus(Boolean status) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(LOGIN_STATUS, status);
        editor.apply();
    }

    public void logout() {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        setLoginStatus(false);
    }


    public SignInResponse getUserData(){
        SharedPreferences prefs = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = prefs.getString(USER_DATA, "");
        return gson.fromJson(json, SignInResponse.class) ;
    }

    public void setUserData(SignInResponse data){
        setToken(data.getToken());
        SharedPreferences.Editor editor = mContext.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE).edit();
        Gson gson = new Gson();
        String json = gson.toJson(data);
        editor.putString(USER_DATA, json);
        setLoginStatus(true);
        editor.apply();
    }

    private void setToken(String token) {
        SharedPreferences sharedPreferences = mContext.getSharedPreferences(SHARED_PREF_NAME,
                0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TOKEN, "Bearer " + token);
        editor.apply();
    }

    public String getToken() {
        final SharedPreferences sharedPreferences = mContext.getSharedPreferences(
                SHARED_PREF_NAME, 0);
        return sharedPreferences.getString(TOKEN, "");
    }
}
