package com.youssefelsa3ed.thetask.ui.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.youssefelsa3ed.thetask.R;
import com.youssefelsa3ed.thetask.databinding.ActivityLoginBinding;
import com.youssefelsa3ed.thetask.ui.main.MainActivity;
import com.youssefelsa3ed.thetask.utills.ParentClass;
import com.youssefelsa3ed.thetask.utills.SharedPrefManager;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private ActivityLoginBinding loginBinding;
    private LoginViewModel loginViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        loginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        loginBinding.login.setOnClickListener(this);
        loginViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
        loginViewModel.getResponseLiveData().observe(this, response -> {
            switch (response.getStatus()){
                case Loading:
                    loginBinding.loading.setVisibility(View.VISIBLE);
                    break;
                case Failure:
                    loginBinding.loading.setVisibility(View.GONE);
                    if (response.getError().getThrowable() == null)
                        ParentClass.makeToast(this, response.getError().getMsg());
                    else
                        ParentClass.handleException(this, response.getError().getThrowable());
                    break;
                case Success:
                    loginBinding.loading.setVisibility(View.GONE);
                    SharedPrefManager sharedPrefManager = new SharedPrefManager(this);
                    sharedPrefManager.setUserData(response.getData());
                    startActivity(new Intent(this, MainActivity.class));
                    break;
            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.login){
            if(validate())
                loginViewModel.SignIn(loginBinding.txtMobileNumber.getText().toString(),loginBinding.txtPassword.getText().toString(),this);
        }
    }

    private boolean validate() {
        if(loginBinding.txtMobileNumber.getText().toString().isEmpty()){
            loginBinding.txtMobileNumber.setError(getResources().getString(R.string.required_field));
            loginBinding.txtMobileNumber.requestFocus();
            return false;
        }
        if(loginBinding.txtPassword.getText().toString().isEmpty()){
            loginBinding.txtPassword.setError(getResources().getString(R.string.required_field));
            loginBinding.txtPassword.requestFocus();
            return false;
        }
        return true;
    }
}
