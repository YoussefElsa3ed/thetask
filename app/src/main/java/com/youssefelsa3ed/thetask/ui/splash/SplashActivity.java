package com.youssefelsa3ed.thetask.ui.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.CountDownTimer;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.youssefelsa3ed.thetask.R;
import com.youssefelsa3ed.thetask.ui.main.MainActivity;
import com.youssefelsa3ed.thetask.ui.login.LoginActivity;
import com.youssefelsa3ed.thetask.utills.SharedPrefManager;

import java.util.Locale;

public class SplashActivity extends AppCompatActivity {

    private String language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Fresco.initialize(this);
        readSharedPreference();
    }

    private void readSharedPreference() {
        SharedPreferences prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        language = prefs.getString("language", "ar");
        setLanguages();
        loading();
    }

    public void loading() {
        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                SharedPrefManager sharedPrefManager = new SharedPrefManager(SplashActivity.this);
                if (sharedPrefManager.getLoginStatus())
                    startActivity(new Intent(SplashActivity.this, MainActivity.class));
                else
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                finish();
            }
        }.start();
    }

    private void setLanguages() {
        Configuration config = new Configuration();
        config.locale = new Locale(language);
        getResources().updateConfiguration(config,getResources().getDisplayMetrics());
    }
}
