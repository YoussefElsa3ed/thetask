package com.youssefelsa3ed.thetask.ui.login;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.youssefelsa3ed.thetask.Remote.ApiUtils;
import com.youssefelsa3ed.thetask.models.GeneralResponse.Error;
import com.youssefelsa3ed.thetask.models.GeneralResponse.GeneralResponseLiveData;
import com.youssefelsa3ed.thetask.models.signInResponse.SignInResponse;
import com.youssefelsa3ed.thetask.utills.ParentClass;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginViewModel extends AndroidViewModel {
    private GeneralResponseLiveData<SignInResponse> responseLiveData;

    public LoginViewModel(@NonNull Application application) {
        super(application);
        responseLiveData = new GeneralResponseLiveData<>();
    }

    void SignIn(String mobileNumber, String password, Context context){
        responseLiveData.postLoading();
        ApiUtils.getAuthNetwork().SignIn(mobileNumber, password).enqueue(new Callback<SignInResponse>() {
            @Override
            public void onResponse(@NonNull Call<SignInResponse> call, @NonNull Response<SignInResponse> response) {
                if(response.isSuccessful()){
                    if(Objects.requireNonNull(response.body()).isSuccess()){
                        responseLiveData.postSuccess(response.body());
                    }
                    else
                        responseLiveData.postError(new Error(ParentClass.getLocalization(context).equals("ar") ?
                                response.body().getArabicMessage():
                                response.body().getEnglishMessage(), null));
                }
                else
                    responseLiveData.postError(new Error(response.message(), null));
            }

            @Override
            public void onFailure(@NonNull Call<SignInResponse> call, @NonNull Throwable t) {
                responseLiveData.postError(new Error(null, t));
            }
        });
    }

    GeneralResponseLiveData<SignInResponse> getResponseLiveData() {
        return responseLiveData;
    }
}
