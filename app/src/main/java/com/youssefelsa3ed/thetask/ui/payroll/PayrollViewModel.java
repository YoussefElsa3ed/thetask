package com.youssefelsa3ed.thetask.ui.payroll;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.youssefelsa3ed.thetask.Remote.ApiUtils;
import com.youssefelsa3ed.thetask.models.GeneralResponse.Error;
import com.youssefelsa3ed.thetask.models.GeneralResponse.GeneralResponseLiveData;
import com.youssefelsa3ed.thetask.models.payRollResponse.PayRollResponse;
import com.youssefelsa3ed.thetask.models.payRollResponse.Payroll;
import com.youssefelsa3ed.thetask.utills.ParentClass;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PayrollViewModel extends AndroidViewModel {

    private GeneralResponseLiveData<Payroll> responseLiveData;

    public PayrollViewModel(@NonNull Application application) {
        super(application);
        responseLiveData = new GeneralResponseLiveData<>();
    }

    void getPayRoll(String token, Context context) {
        responseLiveData.postLoading();
        ApiUtils.getPayrollNetwork().getPayroll(token).enqueue(new Callback<PayRollResponse>() {
            @Override
            public void onResponse(@NonNull Call<PayRollResponse> call, @NonNull Response<PayRollResponse> response) {
                if(response.isSuccessful()){
                    if(Objects.requireNonNull(response.body()).isSuccess()){
                        responseLiveData.postSuccess(response.body().getPayroll());
                    }
                    else
                        responseLiveData.postError(new Error(ParentClass.getLocalization(context).equals("ar") ?
                                response.body().getArabicMessage():
                                response.body().getEnglishMessage(), null));
                }
                else
                    responseLiveData.postError(new Error(response.message(), null));
            }

            @Override
            public void onFailure(@NonNull Call<PayRollResponse> call, @NonNull Throwable t) {
                responseLiveData.postError(new Error(null, t));
            }
        });
    }

    GeneralResponseLiveData<Payroll> getResponseLiveData() {
        return responseLiveData;
    }
}