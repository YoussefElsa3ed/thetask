package com.youssefelsa3ed.thetask.ui.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.youssefelsa3ed.thetask.R;
import com.youssefelsa3ed.thetask.databinding.SalaryBinding;
import com.youssefelsa3ed.thetask.models.payRollResponse.SalarySlipsItem;
import com.youssefelsa3ed.thetask.utills.ParentClass;

import java.util.Locale;

public class SalarySlipAdapter extends ListAdapter<SalarySlipsItem, SalarySlipAdapter.ViewHolder> {

    private Context context;

    public SalarySlipAdapter() {
        super(SalarySlipsItem.DIFF_CALLBACK);
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).isAllowance() ? 0 : 1;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        return new ViewHolder(DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_item, parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.salaryBinding.txtNumber.setText(String.format(Locale.US,"%d", holder.getAdapterPosition() + 1));
        holder.salaryBinding.txtValue.setText(String.format(Locale.US,"%s %s", getItem(holder.getAdapterPosition()).getSALVALUE(), context.getResources().getString(R.string.egp)));
        holder.salaryBinding.txtClause.setText(
                ParentClass.getLocalization(context).equals("ar") ?
                getItem(holder.getAdapterPosition()).getCOMPDESCAR() :
                getItem(holder.getAdapterPosition()).getCOMPDESCEN()
        );
        if(getItemViewType(holder.getAdapterPosition()) == 0){
            if(holder.getAdapterPosition() % 2 == 0)
                holder.salaryBinding.layoutParent.setBackgroundColor(context.getResources().getColor(R.color.colorWhite));
            else
                holder.salaryBinding.layoutParent.setBackgroundColor(context.getResources().getColor(R.color.colorBabyBlue));
        }
        else {
            holder.salaryBinding.txtValue.setPaintFlags(holder.salaryBinding.txtValue.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            holder.salaryBinding.layoutParent.setBackgroundColor(context.getResources().getColor(R.color.colorPink));
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        private SalaryBinding salaryBinding;
        ViewHolder(@NonNull SalaryBinding salaryBinding) {
            super(salaryBinding.getRoot());
            this.salaryBinding = salaryBinding;
        }
    }
}
