package com.youssefelsa3ed.thetask.ui.payroll;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.youssefelsa3ed.thetask.R;
import com.youssefelsa3ed.thetask.databinding.FragmentPayrollBinding;
import com.youssefelsa3ed.thetask.models.payRollResponse.EmployeeData;
import com.youssefelsa3ed.thetask.models.payRollResponse.Payroll;
import com.youssefelsa3ed.thetask.models.payRollResponse.SalarySlipsItem;
import com.youssefelsa3ed.thetask.ui.adapter.SalarySlipAdapter;
import com.youssefelsa3ed.thetask.utills.ParentClass;
import com.youssefelsa3ed.thetask.utills.SharedPrefManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;


public class PayrollFragment extends Fragment {

    private PayrollViewModel payrollViewModel;
    private FragmentPayrollBinding payrollBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        payrollViewModel = new ViewModelProvider(this).get(PayrollViewModel.class);
        payrollViewModel.getResponseLiveData().observe(this, response -> {
            switch (response.getStatus()){
                case Loading:
                    payrollBinding.progressBar.setVisibility(View.VISIBLE);
                    break;
                case Failure:
                    payrollBinding.progressBar.setVisibility(View.GONE);
                    if (response.getError().getThrowable() == null)
                        ParentClass.makeToast(getContext(), response.getError().getMsg());
                    else
                        ParentClass.handleException(getContext(), response.getError().getThrowable());
                    break;
                case Success:
                    payrollBinding.progressBar.setVisibility(View.GONE);
                    fillView(response.getData());
                    break;
            }
        });
    }

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        payrollBinding =  DataBindingUtil.inflate(inflater, R.layout.fragment_payroll, container, false);
        return payrollBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        payrollViewModel.getPayRoll(new SharedPrefManager(getContext()).getToken(), getContext());
    }

    private void fillView(Payroll data) {
        if(data.getEmployee().size() > 0){
            EmployeeData currentEmp = data.getEmployee().get(0);
            payrollBinding.txtEmpDataName.setText(ParentClass.getLocalization(Objects.requireNonNull(getContext())).equals("ar") ?
                    currentEmp.getEMPDATAAR() :
                    currentEmp.getEMPDATAEN());
            payrollBinding.txtEmpDataJob.setText(ParentClass.getLocalization(Objects.requireNonNull(getContext())).equals("ar") ?
                    currentEmp.getJOBNAMEAR() :
                    currentEmp.getJOBNAMEEN());
            payrollBinding.txtDate.setText(data.getDate());
            List<SalarySlipsItem> empSalarySlib = new ArrayList<>();
            float allowances = 0f, deductions = 0f, allowancesPercentage, deductionsPercentage, sum, totalSalary;
            for (SalarySlipsItem item: data.getAllowances())  {
                if(item.getEMPID() == currentEmp.getEMPID()){
                    item.setAllowance(true);
                    empSalarySlib.add(item);
                    allowances += item.getSALVALUE();
                }
            }
            for (SalarySlipsItem item: data.getDeduction())  {
                if(item.getEMPID() == currentEmp.getEMPID()){
                    item.setAllowance(false);
                    empSalarySlib.add(item);
                    deductions += item.getSALVALUE();
                }
            }
            sum = allowances + deductions;
            allowancesPercentage = allowances / sum * 100;
            deductionsPercentage = deductions / sum * 100;
            totalSalary = allowances - deductions;
            payrollBinding.txtEmpDataTotalSalary.setText(String.format(Locale.US,"%.2f %s", totalSalary, getResources().getString(R.string.egp)));
            if(currentEmp.getEMPPIC() != null && !currentEmp.getEMPPIC().isEmpty())
                payrollBinding.empPic.setImageURI(currentEmp.getEMPPIC());

            payrollBinding.txtAllowances.setText(String.format(Locale.US,"%.2f %s", allowances, getResources().getString(R.string.egp)));
            payrollBinding.txtDeductions.setText(String.format(Locale.US,"%.2f %s", deductions, getResources().getString(R.string.egp)));
            payrollBinding.txtSum.setText(String.format(Locale.US,"%.2f %s", totalSalary, getResources().getString(R.string.egp)));

            //draw Chart
            List<PieEntry> entries = new ArrayList<>();
            PieDataSet set = new PieDataSet(entries, "");
            List<Integer> usedColorsList = new ArrayList<>();
            usedColorsList.add(getResources().getColor(R.color.colorChartFirst));
            usedColorsList.add(getResources().getColor(R.color.colorChartSecond));
            entries.add(new PieEntry(allowancesPercentage, String.format(Locale.US,"%.1f %s", allowancesPercentage, "%")));
            entries.add(new PieEntry(deductionsPercentage, String.format(Locale.US,"%.1f %s", deductionsPercentage, "%")));
            set.setDrawValues(false);
            set.setSelectionShift(0f);
            set.setSliceSpace(3f);
            set.setColors(usedColorsList);
            PieData pieData = new PieData(set);
            payrollBinding.chart.getLegend().setEnabled(false);
            payrollBinding.chart.setData(pieData);
            payrollBinding.chart.setEntryLabelTextSize(12f);
            payrollBinding.chart.getDescription().setText("");
            payrollBinding.chart.setDrawHoleEnabled(false);
            payrollBinding.chart.invalidate();

            SalarySlipAdapter salarySlipAdapter = new SalarySlipAdapter();
            payrollBinding.rvSalarySlib.setAdapter(salarySlipAdapter);
            salarySlipAdapter.submitList(empSalarySlib);
        }
    }
}