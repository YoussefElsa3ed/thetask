package com.youssefelsa3ed.thetask.Remote.Services;

import com.youssefelsa3ed.thetask.models.signInResponse.SignInResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface AuthNetwork {

    @FormUrlEncoded
    @POST("SignIn")
    Call<SignInResponse> SignIn(
            @Field("MobileNumber") String MobileNumber,
            @Field("Password") String Password
    );
}
