package com.youssefelsa3ed.thetask.Remote;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class RetrofitClient {

    private static Retrofit retrofit = null;


    static Retrofit getClient() {

        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://40.127.194.127:7777/api/Salamtak/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(setTime())
                    .build();
        }
        return retrofit;
    }

    private static OkHttpClient setTime() {
        return new OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build();
    }
}
