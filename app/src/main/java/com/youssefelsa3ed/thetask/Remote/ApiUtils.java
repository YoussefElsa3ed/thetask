package com.youssefelsa3ed.thetask.Remote;

import com.youssefelsa3ed.thetask.Remote.Services.AuthNetwork;
import com.youssefelsa3ed.thetask.Remote.Services.PayrollNetwork;

public class ApiUtils {

    public static AuthNetwork getAuthNetwork(){
        return RetrofitClient.getClient().create(AuthNetwork.class);
    }

    public static PayrollNetwork getPayrollNetwork(){
        return RetrofitClient.getClient().create(PayrollNetwork.class);
    }
}
