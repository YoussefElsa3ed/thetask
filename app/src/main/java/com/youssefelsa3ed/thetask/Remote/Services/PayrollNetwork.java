package com.youssefelsa3ed.thetask.Remote.Services;

import com.youssefelsa3ed.thetask.models.payRollResponse.PayRollResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

public interface PayrollNetwork {

    @GET("GetPayroll")
    Call<PayRollResponse> getPayroll(
            @Header("Authorization") String token
    );
}
